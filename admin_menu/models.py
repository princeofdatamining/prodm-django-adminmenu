from django.db import models
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.apps import apps
from adminsortable.models import SortableMixin

import numbers

from . import constant

_cached = {}

def content_type_to_model(ct):
    if ct.pk not in _cached:
        _cached[ct.pk] = alias_to_model('{}.{}'.format(ct.app_label, ct.model))
    return _cached[ct.pk]

def alias_to_model(name):
    if name not in _cached:
        _cached[name] = apps.get_model(name)
    return _cached[name]

def any_to_content_type(v):
    if isinstance(v, ContentType):
        return v
    if isinstance(v, type) and issubclass(v, models.base.Model):
        return ContentType.objects.get(app_label=v._meta.app_label, model=v._meta.model_name)
    if isinstance(v, models.base.Model):
        return ContentType.objects.get(app_label=v.__class__._meta.app_label, model=v.__class__._meta.model_name)
    if isinstance(v, numbers.Number) or v.isdigit():
        return ContentType.objects.get(pk=v)
    app_label, _, model_name = v.partition('.')
    return ContentType.objects.get(app_label=app_label, model=model_name.lower())

#

class VisibleContentManager(models.Manager):

    def public(self):
        return self.filter(hidden=False)

VisibleContents = VisibleContentManager()

class VisibleContent(models.Model):

    class Meta:
        verbose_name = constant.VISIBLE_CONTENT
        verbose_name_plural = constant.VISIBLE_CONTENT
        ordering = ['priority']

    objects = VisibleContents

    content_type = models.ForeignKey(ContentType, verbose_name=constant.CONTENT_MODEL)
    priority = models.IntegerField(constant.PRIORITY, default=0)
    hidden = models.BooleanField(constant.HIDDEN, default=False)

    def __str__(self): return str(self.content_type)

    @property
    def model_name(self):
        if not hasattr(self, '_alias'):
            self._alias = '{}.{}'.format(self.content_type.app_label, self.content_type.model)
        return self._alias

    @property
    def model_class(self):
        if not hasattr(self, '_model'):
            self._model = content_type_to_model(self.content_type)
        return self._model

#

class SidebarManager(models.Manager):

    def public(self):
        return self.filter(hidden=False)

Sidebars = SidebarManager()

class Sidebar(models.Model):

    class Meta:
        verbose_name = constant.SIDEBAR
        verbose_name_plural = constant.SIDEBAR
        ordering = ['priority']

    objects = Sidebars

    name = models.CharField(constant.TITLE, max_length=63)
    priority = models.IntegerField(constant.PRIORITY, default=0)
    hidden = models.BooleanField(constant.HIDDEN, default=False)

    def __str__(self): return self.name

#

class SubmenuManager(models.Manager):

    def public(self):
        return self.filter(hidden=False)

Submenus = SubmenuManager()

class Submenu(models.Model):

    class Meta:
        verbose_name = constant.MENU
        verbose_name_plural = constant.MENUS
        ordering = ['priority']

    objects = Submenus

    sidebar = models.ForeignKey(Sidebar, verbose_name=constant.SIDEBAR)
    content = models.ForeignKey(VisibleContent, verbose_name=constant.CONTENT_MODEL, limit_choices_to={'hidden': False})
    priority = models.IntegerField(constant.PRIORITY, default=0)
    hidden = models.BooleanField(constant.HIDDEN, default=False)

    def __str__(self): return str(self.content)

#

def rebuild(sidebars):
    if not sidebars:
        return
    vc_ids = [vc.pk for vc in VisibleContents.filter(hidden=False)]
    sb_ids = [sb.pk for sb in Sidebars.filter(hidden=False)]
    sm_ids = [sm.pk for sm in Submenus.filter(hidden=False)]
    index = 0
    for i, (title, models) in enumerate(sidebars):
        sb, _ = Sidebars.update_or_create(defaults=dict(
            priority=i+1, hidden=False,
        ), name=title)
        (sb.pk in sb_ids) and sb_ids.remove(sb.pk)
        for j, ct in enumerate(models):
            ct = any_to_content_type(ct)
            #
            vc, _ = VisibleContents.update_or_create(defaults=dict(
                priority=index+1, hidden=False,
            ), content_type=ct)
            (vc.pk in vc_ids) and vc_ids.remove(vc.pk)
            index += 1
            #
            sm, _ = Submenus.update_or_create(defaults=dict(
                sidebar=sb,
                priority=j+1,
                hidden=False,
            ), content_id=vc.pk)
            (sm.pk in sm_ids) and sm_ids.remove(sm.pk)
    Sidebars.filter(pk__in=sb_ids).update(hidden=True)
    VisibleContents.filter(pk__in=vc_ids).update(hidden=True)
    Submenus.filter(pk__in=sm_ids).update(hidden=True)
