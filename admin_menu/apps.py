from django.apps import AppConfig
from . import constant

class adminMenuAppConfig(AppConfig):

    name = 'admin_menu'
    verbose_name = constant.APP_VERBOSE_NAME

    def ready(self):
        pass
