# Django Admin Menu

Current version: 1.0.1

This project makes it easy to manage sidebar menu in Django admin.

## Supported Django Versions

prodm-django-adminmenu 1.* and higher are compatiable with Django 1.9 or higher.

## Supported Admin Themes

1. `django-wpadmin`
2. `bootstrap_admin`

## Installation

1. Install from source:

	1. Download prodm-django-adminmenu from [source](https://bitbucket.org/princeofdatamining/prodm-django-adminmenu/get/master.zip), then unzip the directory and cd into the uncompressed project directory
	2. Run `$ git clone https://bitbucket.org/princeofdatamining/prodm-django-adminmenu`, then cd into the reposity directory

2. Run `$ python setup.py install`

## Configuration

1. Add `admin_menu` to your `INSTALLED_APPS`

2. Run `$ python manage.py migrate`

## Usage

1. Prepare your admin menus:

	```
	from admin_menu.models import rebuild
	rebuild([
	    ('Authentication', [
	        'auth.Group',
	        'auth.User',
	        # ...
	    ]),
	    # ...
	])
	```
	
2. Load plugins

	```
	# apps.py
	class YourAppConfig(AppConfig):

	    def ready(self):
			# ...
			import admin_menu.plugins
			# ...

	```
	
3. Work with themes:

	1. `django-wpadmin`

		```
		# in your settings.py
		WPADMIN = {
			"admin": {
				'menu': {
					'left': 'qadminMenu.plugins.wpadmin.LeftMenu',
					# ...
				},
				# ...
			},
			# ...
		}
		```

	2. `bootstrap_admin`

		```
		It is works!
		```
