from django.contrib import admin, messages
from django import forms
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.conf import settings
from django.conf.urls import url
from django.utils import timezone
from django.utils.safestring import mark_safe

from adminsortable.admin import SortableAdmin, NonSortableParentAdmin, SortableStackedInline

from . import models

# class MenuInlineAdmin(SortableStackedInline):
class MenuInlineAdmin(admin.StackedInline):
    model = models.Submenu
    extra = 1
    fields = ('content',)

@admin.register(models.Sidebar)
class SidebarAdmin(SortableAdmin):

    list_display = ('name',)
    fields = ('name', 'hidden')
    inlines = [MenuInlineAdmin]

    def get_queryset(self, *args, **kwargs):
        qs = super(SidebarAdmin, self).get_queryset(*args, **kwargs)
        return qs.filter(hidden=False)
