from django.utils.translation import ugettext_lazy as _, ungettext_lazy, pgettext_lazy, npgettext_lazy

APP_VERBOSE_NAME = UPLOAD_APP_VERBOSE_NAME = pgettext_lazy('App', 'AdminMenu')

VISIBLE_CONTENT = pgettext_lazy('Model', 'Model')
SIDEBAR = pgettext_lazy('Model', 'Sidebar')
MENU = pgettext_lazy('Model', 'Sidebar menu')
MENUS = pgettext_lazy('Model', 'Sidebar menus')

TITLE = pgettext_lazy('Sidebar', 'Title')
PRIORITY = pgettext_lazy('Sidebar', 'Priority')
HIDDEN = pgettext_lazy('Sidebar', 'Hidden')
CONTENT_MODEL = pgettext_lazy('Sidebar', 'Model')
