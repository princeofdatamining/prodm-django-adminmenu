
Build Menus:

```
from qadminmenu.models import rebuild

rebuild([
    ('GROUP', [
        # 'auth.Group',
        # ...
    ]),
    # ...
])
```

Admin Plugins:

```
# In `ready` of your project app.
if 'qadminmenu' in settings.INSTALLED_APPS:
    import qadminmenu.plugins
```

Supports:

* wpadmin
* bootstrap_admin
