from django.utils import lru_cache

from ..models import Sidebars, content_type_to_model

@lru_cache.lru_cache()
def load_sidebar():
    return [
        (sidebar, [
            content_type_to_model(submenu.content.content_type)
            for submenu in sidebar.submenu_set.public()
        ])
        for sidebar in Sidebars.public()
    ]

_cached = {}
def check_user_perms(user, request, model, admin):
    if user.pk not in _cached:
        cache = _cached[user.pk] = {}
    else:
        cache = _cached[user.pk]
    #
    app_label = model._meta.app_label
    if app_label not in cache:
        perm = cache[app_label] = user.has_module_perms(app_label)
    else:
        perm = cache[app_label]
    if not perm:
        return False
    #
    info = "%s.%s" % (app_label, model._meta.model_name)
    if info not in cache:
        perm = cache[info] = admin.get_model_perms(request)
    else:
        perm = cache[info]
    if not perm:
        return False
    return perm
