from django.conf import settings

if 'bootstrap_admin' in settings.INSTALLED_APPS:
    from . import bootstrap_admin

if 'wpadmin' in settings.INSTALLED_APPS:
    from . import wpadmin
