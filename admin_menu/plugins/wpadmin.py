from django.contrib.admin import site
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse, NoReverseMatch
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.text import capfirst

from . import base

from wpadmin.utils import get_wpadmin_settings, get_admin_site_name

def get_settings(key, **kwargs):
    settings = get_wpadmin_settings(**kwargs)
    partitions = key.split('.')
    for node in partitions[:-1]: settings = settings.get(node, {})
    return settings.get(partitions[-1])

### customize menu ###

from wpadmin.menu.items import MenuItem as _MenuItem
from wpadmin.menu.menus import BasicLeftMenu

class MenuItem(_MenuItem):

    def is_selected(self, request):
        return getattr(self, 'selected', False)

class MenuList(MenuItem):

    def is_empty(self):
        return len(self.children) == 0

class LeftMenu(BasicLeftMenu):

    def init_with_context(self, context):
        request = context.get('request')
        user = context.get('user')
        if not self.is_user_allowed(user):
            return
        #
        admin_site_name = get_admin_site_name(context)
        if get_settings('dashboard.left'):
            self.children.append(MenuItem(
                    title=_('Dashboard'),
                    icon='fa-tachometer',
                    description=_('Dashboard'),
                    url=url,
                    selected=url == request.path,
            ))
        #
        for (sidebar, submenu_models) in base.load_sidebar():
            menu_list = MenuList(
                title=_(sidebar.name),
                icon='fa-tasks',
                description=_(sidebar.name),
                url='javascript:void(0)',
            )
            for model in submenu_models:
                perms = base.check_user_perms(user, request, model, site._registry[model])
                if not perms or True not in perms.values():
                    continue
                info = (model._meta.app_label, model._meta.model_name)
                model_dict = {
                    'title': capfirst(model._meta.verbose_name_plural),
                    'description': _("Change" if perms['change'] else "No permission"),
                    'url': False,
                    'add_url': False,
                    'selected': False,
                }
                if perms.get('change'):
                    try:
                        model_dict['url'] = url = reverse(
                            'admin:%s_%s_changelist' % info,
                            current_app=site.name,
                        )
                        if url in request.path:
                            model_dict['selected'] = True
                            menu_list.selected = True
                    except NoReverseMatch:
                        pass
                if perms.get('add'):
                    try:
                        model_dict['add_url'] = reverse(
                            'admin:%s_%s_add' % info,
                            current_app=site.name,
                        )
                    except NoReverseMatch:
                        pass
                menu_list.children.append(MenuItem(**model_dict))
            if not menu_list.children:
                continue
            self.children.append(menu_list)
        #

### fix bugs ###

from wpadmin.templatetags.wpadmin_tags import (register, 
    wpadmin_render_custom_style,
)
def fix_wpadmin_render_custom_style(context):
    return mark_safe(wpadmin_render_custom_style(context))
register.simple_tag(name='wpadmin_render_custom_style', takes_context=True)(fix_wpadmin_render_custom_style)

### customize template ###

from wpadmin.templatetags.wpadmin_menu_tags import (register, 
    wpadmin_render_left_menu, wpadmin_render_menu_top_item, wpadmin_render_menu_item,
    wpadmin_render_top_menu, wpadmin_render_user_tools, gravatar_url,
)

def reset_register(func, settings, default):
    settings = get_settings(settings)
    if settings and not isinstance(settings, str): settings = default
    settings and register.inclusion_tag(settings, takes_context=True)(func)

reset_register(wpadmin_render_left_menu, 'templates.left_menu', 'wpadmin/menu/left_menu_customize.html')
reset_register(wpadmin_render_menu_top_item, 'templates.menu_top', 'wpadmin/menu/top_item_expand.html')
reset_register(wpadmin_render_menu_item, 'templates.menu_item', 'wpadmin/menu/menu_item_customize.html')
reset_register(wpadmin_render_user_tools, 'templates.user_tools', 'wpadmin/menu/user_tools_customize.html')
