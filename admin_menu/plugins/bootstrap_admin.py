from django.contrib.admin import site
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.text import capfirst
from django.conf import settings

from . import base

def make_menus(context):
    app_list = []
    user = context.get('user')
    request = context.get('request')
    admin_url = reverse('admin:index')
    for (sidebar, submenu_models) in base.load_sidebar():
        models = []
        selected = False
        for model in submenu_models:
            model_admin = site._registry[model]
            #
            app_label = model._meta.app_label
            perms = base.check_user_perms(user, request, model, model_admin)
            if not perms or True not in perms.values():
                continue
            info = (app_label, model._meta.model_name)
            model_dict = {
                'name': capfirst(model._meta.verbose_name_plural),
                'object_name': model._meta.object_name,
                'perms': perms,
            }
            models.append(model_dict)
            if perms.get('change'):
                try:
                    model_dict['admin_url'] = url = reverse(
                        'admin:%s_%s_changelist' % info,
                        current_app=site.name,
                    )
                    if url in request.path: selected = True
                except NoReverseMatch:
                    pass
            if perms.get('add'):
                try:
                    model_dict['add_url'] = reverse(
                        'admin:%s_%s_add' % info,
                        current_app=site.name,
                    )
                except NoReverseMatch:
                    pass
        models and app_list.append({
            'name': sidebar.name,
            'app_label': sidebar.name,
            'app_url': admin_url if selected else 'javascript:void(0)',
            'has_module_perms': True,
            'models': models,
        })
    return {'app_list': app_list, 'current_url': request.path}

from bootstrap_admin.templatetags.bootstrap_admin_template_tags import register

register.inclusion_tag(
    getattr(settings, 'BOOTSTRAP_ADMIN_SIDEBAR_MENU_TEMPLATE', None) or 'bootstrap_admin/sidebar_menu.html',
    takes_context=True, name='render_menu_app_list'
)(make_menus)
